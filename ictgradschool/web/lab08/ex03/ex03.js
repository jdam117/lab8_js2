"use strict";

// FUNCTIONS
// ------------------------------------------

// TODO Complete this function, which should generate and return a random number between a given lower and upper bound (inclusive)
function getRndInteger(min, max) {
   var number = Math.floor(Math.random() * (max - min) + min);
    // max - min);

   return number;
}
console.log("getRndInteger is: " + getRndInteger(4, 12));

// TODO Complete this function, which should round the given number to 2dp and return the result.
function roundTo2dp(number) {
    number *= 100;
    number = Math.round(number);
    number /= 100;

    return number;
}

console.log("roundTo2dp is : " + roundTo2dp(15.53252645));

// TODO Write a function which calculates and returns the volume of a cone.

function coneVolume(height, radius){
    var volume = Math.PI * (radius * radius) * (height / 3);
    return volume;
}

console.log("coneVolume is : " + coneVolume(15, 22));

// TODO Write a function which calculates and returns the volume of a cylinder.
function cylinderVolume(height, radius){
    var volume = Math.PI * (radius * radius) * height;
    return volume;
}
console.log("cylinderVolume is : " + cylinderVolume(15, 22));


// TODO Write a function which prints the name and volume of a shape, to 2dp.
function printShape(shape, volume){
    return "The volume of the " + shape + " is: " + roundTo2dp(volume) + " cm^3";
}
console.log(printShape("Cylinder", 5701.990666265475));
// ------------------------------------------

// TODO Complete the program as detailed in the handout. You must use all the functions you've written appropriately.

function largerShape(shape1, shape1Name, shape2, shape2Name){
    if(shape1 > shape2){
        console.log("Where " + shape1Name + "is the name of the shape with the larger volume, and " + shape1 + " is its volume, to two decimal places.")
    }else if(shape1 < shape2){
        console.log("Where " + shape2Name + "is the name of the shape with the larger volume, and " + shape2 + " is its volume, to two decimal places.")
    }else{
        console.log("Both shapes are equal in size");
    }
}


console.log("--------------------------------------------------------------------------------------")
console.log("Complete Program");
console.log("--------------------------------------------------------------------------------------")

//Cone dimensions
var randomConeHeight = getRndInteger(25,50);
var randomConeRadius = getRndInteger(25,50);
//Cylinder dimensions
var randomCylinderHeight = getRndInteger(25,50);
var randomCylinderRadius = getRndInteger(25,50);

var generatedConeVolume = coneVolume(randomConeHeight, randomConeRadius);
var generatedCylinderVolume= cylinderVolume(randomCylinderHeight, randomCylinderRadius);

console.log(printShape("Cone", roundTo2dp(generatedConeVolume)));
console.log(printShape("Cylinder",roundTo2dp(generatedCylinderVolume)));

console.log(largerShape(roundTo2dp(generatedConeVolume), "Cone", roundTo2dp(generatedCylinderVolume),"Cylinder"));